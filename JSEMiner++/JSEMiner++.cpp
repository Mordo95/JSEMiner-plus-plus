// JSEMiner++.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <chrono>
#include <string>
#include "JSEMiner.h"
#include <fstream>
#include <exception>
#include <regex>
#include <Windows.h>
#include <thread>
#include <iostream>

using namespace JSEMiner;

int main()
{
	int threads = 0;
	std::string apiKey = "";
	std::cout.sync_with_stdio(false);
	std::ifstream f("./settings.json");
	if (!f.good()) {
		std::cout << "No settings file found!";
		std::cin.get();
		return 0;
	}
	try {
		json j;
		f >> j;
		threads = j["threads"].get<int>();
		apiKey = j["apikey"].get<std::string>();
	}
	catch(std::exception&) {}
	if (threads == 0) {
		std::cout << "No threads defined or is 0. Minimum is 1.\n";

		// Get the amount of cores
		std::cout << "Detecting amount of CPU threads";
		int cpuCores = std::thread::hardware_concurrency();
		// Ask the user if the detected amount of cores is correct
		std::cout << "\nFound " << cpuCores << " threads.\nIs this correct? [y/N]";
		char userInput;
		std::cin >> userInput;
		switch (userInput) {
			// user enters "Y" ot "y"
			case 'Y':
			case 'y':
				threads = cpuCores;
				break;
			// user enters "N" ot "n"
			case 'N':
			case 'n':
				int userCores;
				std::cout << "Please enter the correct amount of threads in your PC: ";
				// TODO: verify if an integer
				std::cin >> userCores;
				threads = userCores;
				break;
		}

		// check if the threads is more than 0, else default to 1
		if (threads == 0) {
			std::cout << "Defaulting to 1 thread";
			threads = 1;
		}

		// write the settings to the settings file
		std::ofstream settingsFile("./settings.json");
		json j{ { "threads", threads }, { "apikey", apiKey } };
		settingsFile << j;
		settingsFile.close();
	}
	// Let's go!
	Miner m(threads, apiKey);
	m.Start();
	Sleep(INFINITE);
    return 0;
}