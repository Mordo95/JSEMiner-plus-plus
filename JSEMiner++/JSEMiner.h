#pragma once
#include <string>
#include <chrono>
#include "json.hpp";
using json = nlohmann::json;

namespace JSEMiner {

	const std::string JSE_SERVER = "https://server.jsecoin.com";
	const std::string JSE_LOAD_SERVER = "https://load.jsecoin.com";

	struct JSEMINERBLOCK {
		std::string nonce; //
		std::string hash;//
		std::string previousHash; //
		std::string version; //
		__int64 startTime; //
		int frequency; //
		int difficulty; //
		int blockReference; //
		bool mainChain; //
		std::string server; //
		int blockID; //
		bool open;
	};

	struct JSEHIT {
		int sendHit = 1;
		std::string pubid;
		std::string siteid;
		std::string subid;
	};

	struct JSETRACK {
		std::string pubid;
		std::string siteid;
		std::string subid;
		std::string userip;
		std::string geo;
		std::string useragent;
		std::string device;
		std::string browser;
		std::string os;
		std::string referrer;
		std::string url;
		std::string language;
		std::string uniq;
		int hits;
	};

	struct HASHRESULT {
		std::string hash;
		int nonce;
	};

	struct JSEPOSTBLOCK {
		int blockID;
		std::string hash;
		int nonce;
		std::string pubid;
		std::string siteid;
		std::string subid;
		std::string uniq;
	};

	class Miner {
	private:
		std::string BlockJSON = "";
		std::string lastHash = "";
		int hashesFound = 0;
		bool hashFound = false;
		int lastID = 0;
		int hashes = 0;
		int HashesPerSecond = 0;
		int blocksReceived = 0;
		int numThreads;
		JSEMINERBLOCK CurrentBlock;
		JSETRACK Track;
		time_t startTime;
		std::string apiKey;
		double api_balance;
	public:
		Miner(int numThreads, std::string apiKey = "");
		std::string JSEPostTrack(std::string url, std::string content);
		std::string JSEGet(std::string url);
		void JSEMine();
		void Start();
		bool RequestNewBlock();
		bool PostHit();
		bool ProcessHash(HASHRESULT *res, int t);
		bool DoHash(std::string in, int nonce, HASHRESULT *res);
		void Miner::ticker();
		void PrintInfo();
		void GetAPIBalance();
		long Rand();
	};

	void from_json(const json& j, JSETRACK& p);
	void to_json(json& j, const JSETRACK& p);

	void from_json(const json& j, JSEMINERBLOCK& p);
	void to_json(json& j, const JSEMINERBLOCK& p);

	void from_json(const json& j, JSEHIT& p);
	void to_json(json& j, const JSEHIT& p);

	void to_json(json& j, const JSEPOSTBLOCK& p);
}